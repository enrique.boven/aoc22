package nl.aoc;

import nl.aoc.data.FileReader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Day3 {

    protected final List<String> input = FileReader.getFile("/day3.txt");

    public Day3() {
        partOne();
        partTwo();
    }

    public void partTwo() {
        int total = 0;
        List<String> groups = new ArrayList<>(input);
        for(int i = 0; i < groups.size(); i+= 3) {
            total += getPoints(getMatchingChar(groups.get(i), groups.get(i + 1), groups.get(i + 2)));
        }
        System.out.println(total);
    }

    public void partOne() {
        int total = 0;
        for (String s : input) {
            String comp1 = s.substring(0, s.length() / 2);
            String comp2 = s.substring(s.length() / 2, s.length());
            System.out.println("matching char: " + getMatchingChar(comp1, comp2));
            total += getPoints(getMatchingChar(comp1, comp2));
        }
        System.out.println(total);
    }

    public int getPoints(char c) {
        //if lowercase remove 96 if uppercase remove 38 on ASCII table
        return c > 90 ? ((int) c) - 96 : ((int) c) - 38;
    }

    public char getMatchingChar(String comp1, String comp2) {
        return (char) comp1.chars().filter(x -> comp2.indexOf(x) != -1).findAny().orElse(0);
    }

    public char getMatchingChar(String backpack1, String backpack2, String backpack3) {
        return (char) backpack1.chars().filter(x -> backpack2.indexOf(x) != -1 && backpack3.indexOf(x) != -1).findAny().orElse(0);
    }

    public static void main(String[] args) {
        new Day3();
    }
}
