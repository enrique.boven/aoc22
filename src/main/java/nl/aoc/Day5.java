package nl.aoc;

import nl.aoc.data.FileReader;

import java.util.*;

public class Day5 {

    protected List<String> input = FileReader.getFile("/day5.txt");
    private HashMap<Integer, Stack<Character>> cachedColumns = new HashMap<>();

    public Day5() {
        preRun();
        for (String s : input) {
            //initiate moves
            if (s.contains("move")) {
                String[] aarray = s.split("[^\\d]+");

                int amountOfMoves = 0;
                int from = 0;
                int to = 0;

                for (String value : aarray) {
                    if (!value.isEmpty()) {
                        if (amountOfMoves == 0) {
                            amountOfMoves = Integer.parseInt(value);
                        } else if (from == 0) {
                            from = Integer.parseInt(value);
                        } else {
                            to = Integer.parseInt(value);
                        }
                    }
                }
                //grabPartOne(amountOfMoves, from, to);
                //grabPartTwo(amountOfMoves, from, to);
            } else {
                int currentIndex = 0;
                //cache board
                for (char c : s.toCharArray()) {
                    if (c > 65 && c < 91) {
                        this.cachedColumns.get(currentIndex / 4 + 1).insertElementAt(c, 0);
                    }
                    currentIndex++;
                }
            }
        }
        printBoard();
        System.out.println("==========================");
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 1; i < 10; i++) {
            stringBuilder.append(this.cachedColumns.get(i).pop());
        }
        System.out.println("Result: " + stringBuilder);
    }

    //PART 2:
    public void grabPartTwo(int amount, int fromColumn, int toColumn) {
        List<Character> toMove = new ArrayList<>();
        for(int i = 0; i < amount; i++) {
            toMove.add(this.cachedColumns.get(fromColumn).pop());
        }
        Collections.reverse(toMove);
        for(char c : toMove) {
            this.cachedColumns.get(toColumn).push(c);
        }
    }
    //PART 1:
    public void grabPartOne(int amount, int fromColumn, int toColumn) {
        for (int i = 0; i < amount; i++) {
            this.cachedColumns.get(toColumn).add(this.cachedColumns.get(fromColumn).pop());
        }
    }
    public void printBoard() {
        for (int i : this.cachedColumns.keySet()) {
            //System.out.println("Column: " + i);
            StringBuilder stringBuilder = new StringBuilder();
            for (char c : this.cachedColumns.get(i)) {
                String f = c + " ";
                stringBuilder.append(f);
            }
            System.out.println(stringBuilder);
        }
    }
    public void preRun() {
        for (int i = 1; i < 10; i++) {
            this.cachedColumns.put(i, new Stack<>());
        }
    }
    public static void main(String[] args) {
        new Day5();
    }
}
