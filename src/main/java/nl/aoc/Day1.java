package nl.aoc;

import nl.aoc.data.FileReader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Day1 {

    protected final List<String> input = FileReader.getFile("/day1.txt");
    public Day1() {

        //part 1
        ArrayList<Integer> totals = new ArrayList<>();
        int total = 0;
        for(String s : input) {
            if(!s.isEmpty()) {
                total += Integer.parseInt(s);
            }else{
                totals.add(total);
                total = 0;
            }
        }
        //part 2:
        totals.sort(Collections.reverseOrder());
        System.out.println("Total of top 3: " + (totals.get(0) + totals.get(1) + totals.get(2)));
    }
    public static void main(String[] args) {
        new Day1();
    }
}
