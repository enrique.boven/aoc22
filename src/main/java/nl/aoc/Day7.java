package nl.aoc;

import nl.aoc.data.FileReader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;

public class Day7 {

    protected final List<String> input = FileReader.getFile("/day7.txt");

    private Directory currentDirectory;
    private List<Directory> directories = new ArrayList<>();

    public Day7() {
        boolean checkingFiles = false;
        for (String s : input) {
            if (checkingFiles) {
                System.out.println("Still checking files");
                if (s.contains("$ cd")) {
                    checkingFiles = false;
                    if (getDirectory(s.split(" ")[2]) != null) {
                        switchDirectory(s.split(" ")[2]);
                    }
                }
                if (s.contains("dir")) {
                    Directory dir = new Directory(s.split(" ")[1], currentDirectory);
                    directories.add(dir);
                    currentDirectory.addDirectory(getDirectory(s.split(" ")[1]));
                    System.out.println("Added dir " + s.split(" ")[1] + " to " + currentDirectory.getName());
                    System.out.println("added directory to list: " + s.split(" ")[1] + " parent " + currentDirectory.getName());

                }
                if (containsNumbers(s)) {
                    System.out.println("Added file " + s + " to " + currentDirectory.getName());
                    currentDirectory.addFile(new File(s, getNumbers(s)));
                }
            } else {
                if (s.contains("$ cd")) {
                    //System.out.println("CD FOUND: " + (currentDirectory == null ? "" : currentDirectory.getName()));
                    if (s.contains("..")) {
                        System.out.println("Going up a directory!");
                        System.out.println("CURRENT " + currentDirectory.getName());
                        //logic for switching to parent
                        switchDirectory(currentDirectory.getParent().getName());
                    } else {
                        switchDirectory(s.split(" ")[2]);
                    }
                }
                if (s.contains("$ ls")) {
                    checkingFiles = true;
                }
            }
        }
        for (Directory directory : this.directories) {
            System.out.println(directory.name + " has " + directory.getFiles().size() + " files and " + directory.getDirs().size() + " directories");
        }
        printData();
    }

    public void printData() {
        int total = 0;
        for (Directory directory : this.directories) {
            if (directory.getTotalFileSize() <= 100000) {
                total += directory.getTotalFileSize();
            }
            System.out.println(directory.getName() + " " + directory.getTotalFileSize());
        }
        System.out.println(total);
    }

    public void switchDirectory(String name) {
        if (getDirectory(name) == null) {
            this.directories.add(new Directory(name, null));
        }
        System.out.println("Switching directory... " + name);
        currentDirectory = getDirectory(name);
    }

    public Directory getDirectory(String name) {
        for (Directory d : directories) {
            if (d.getName().equals(name)) {
                return d;
            }
        }
        return null;
    }

    public void setCurrentDirectory(Directory currentDirectory) {
        this.currentDirectory = currentDirectory;
    }

    public int getNumbers(String s) {
        return Integer.parseInt(s.replaceAll("[^0-9]", ""));
    }

    public static void main(String[] args) {
        new Day7();
    }

    public class File {
        private String name;
        private int size;

        public File(String name, int size) {
            this.name = name;
            this.size = size;
        }

        public String getName() {
            return name;
        }

        public int getSize() {
            return size;
        }
    }

    //write a function with string input that returns true if the string contains numbers
    public boolean containsNumbers(String s) {
        return s.matches(".*\\d+.*");
    }

    public class Directory {

        private Directory parent;
        private String name;
        private List<Directory> dirs;
        private List<File> files;

        public Directory(String name, Directory parent) {
            this.parent = parent;
            this.name = name;
            this.files = new ArrayList<>();
            this.dirs = new ArrayList<>();
        }


        public int getTotalFileSize() {
            int total = 0;
            for (Directory directory : this.dirs) {
                for (Directory d2 : directory.getDirs()) {
                    for (File file : d2.getFiles()) {
                        total += file.getSize();
                    }
                }
                for (File file : directory.getFiles()) {
                    total += file.getSize();
                }
            }
            for (File file : this.files) {
                total += file.size;
            }
            return total;
        }

        public void showFiles() {
            for (Directory directory : this.dirs) {
                System.out.println(directory.getName() + " (dir)");
                for (Directory directory1 : directory.getDirs()) {
                    System.out.println(" " + directory1.getName() + " (dir)");
                    for (File file : directory1.getFiles()) {
                        System.out.println("   " + file.getName() + " (file, size = " + file.getSize() + ")");
                    }
                }
                for (File file : directory.getFiles()) {
                    System.out.println(" " + file.getName() + " (file, size = " + file.getSize() + ")");
                }
            }
        }

        public void addDirectory(Directory directory) {
            //System.out.println("Pushing " + directory.name + " to " + this.name);
            this.dirs.add(directory);
        }

        public void addFile(File file) {
            this.files.add(file);
        }

        public List<Directory> getDirs() {
            return dirs;
        }

        public List<File> getFiles() {
            return files;
        }

        public String getName() {
            return name;
        }

        public Directory getParent() {
            return parent;
        }
    }
}

