package nl.aoc.accesories;

public enum GameResult {
    //X = Lost Y = Draw Z = Win
    X, Y, Z;
}