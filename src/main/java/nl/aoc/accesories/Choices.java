package nl.aoc.accesories;

public enum Choices{

    // X = Rock Y = Paper Z = Scissors
    // A = Rock B = Paper C = Scissors
    // ABC = opponent XYZ = You

    A(1), B(2), C(3), X(1), Y(2), Z(3);
    private final int score;

    Choices(int score) {
        this.score = score;
    }

    public int getScore() {
        return score;
    }
}