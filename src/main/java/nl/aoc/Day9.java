package nl.aoc;

import nl.aoc.data.FileReader;

import java.util.List;

public class Day9 {

    protected final List<String> input = FileReader.getFile("/day9.txt");

     private Position hPos;

    private final String[] initialInput = {
            "......",
            "......",
            "......",
            "......",
            "H....."
    };

    char[][] grid = new char[5][6];

    public Day9() {
        int currentColumn = 0;
        int currentRow = 0;
        for(String s : initialInput) {
            for(char c : s.toCharArray()) {
                if(c == 'H') {
                    hPos = new Position(currentColumn, currentRow);
                }
                grid[currentRow][currentColumn] = c;
                currentColumn++;
            }
            currentRow++;
            currentColumn = 0;
        }

        for(String s : input) {
            Direction dir = Direction.valueOf(s.split(" ")[0]);
            int steps = Integer.parseInt(s.split(" ")[1]);
            System.out.println("Moving " + dir + " " + steps + " steps");
            move(dir, steps);
            //System.out.println("Current hpos " + hPos.toString());
            showGrid();
        }
    }

    public void showGrid() {
        for(int x = 0; x < grid.length; x++) {
            for(int y = 0; y < grid.length; y++) {
                if(hPos.equals(Position.valueOf(x, y))) {

                }
                System.out.print(grid[x][y] + " ");
            }
            System.out.println();
        }
    }

    public class Position {

        private int x;
        private int y;

        public Position(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public boolean equals(Object obj) {
            return this.x == (Position) obj. && this.y == position.y;
        }

        @Override
        public String toString() {
            return "X: " + x + " Y: " + y;
        }

        public void moveUp() {
            this.y--;
        }

        public void moveDown() {
            this.y++;
        }

        public void moveRight() {
            this.x++;
        }

        public void moveLeft() {
            this.x--;
        }

        public int getY() {
            return y;
        }

        public int getX() {
            return x;
        }
    }

    public void move(Direction direction, int amount) {
        switch(direction) {
            default:
            case U:
                for(int i = 0; i < amount; i++) {
                    hPos.moveUp();
                }
                break;
            case D:
                for(int i = 0; i < amount; i++) {
                    hPos.moveDown();
                }
                break;
            case L:
                for(int i = 0; i < amount; i++) {
                    hPos.moveLeft();
                }
                break;
            case R:
                for (int i = 0; i < amount; i++) {
                    hPos.moveRight();
                }
                break;
        }
    }

    public enum Direction {
        U, R, D, L
    }

    public static void main(String[] args) {
        new Day9();
    }
}
