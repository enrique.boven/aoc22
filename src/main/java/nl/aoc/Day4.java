package nl.aoc;

import nl.aoc.data.FileReader;

import java.util.List;

public class Day4 {

    protected final List<String> input = FileReader.getFile("/day4.txt");

    public Day4() {
        partOne();
        partTwo();
    }

    public void partOne() {
        int total = 0;
        for (String s : input) {
            //section 1
            int lowerBound1 = Integer.parseInt(s.split(",")[0].split("-")[0]);
            int upperBound1 = Integer.parseInt(s.split(",")[0].split("-")[1]);
            //section 2
            int lowerBound2 = Integer.parseInt(s.split(",")[1].split("-")[0]);
            int upperBound2 = Integer.parseInt(s.split(",")[1].split("-")[1]);

            if ((lowerBound1 >= lowerBound2 && upperBound1 <= upperBound2)
                    || (lowerBound2 >= lowerBound1 && upperBound2 <= upperBound1)) {
                total++;
            }
        }
        System.out.println(total);
    }

    public void partTwo() {
        int total = 0;
        for (String s : input) {
            //section 1
            int lowerBound1 = Integer.parseInt(s.split(",")[0].split("-")[0]);
            int upperBound1 = Integer.parseInt(s.split(",")[0].split("-")[1]);
            //section 2
            int lowerBound2 = Integer.parseInt(s.split(",")[1].split("-")[0]);
            int upperBound2 = Integer.parseInt(s.split(",")[1].split("-")[1]);

            aapje:
            for(int i = lowerBound1; i <= upperBound1; i++) {
                //System.out.println("Checking " + i);
                for(int j = lowerBound2; j <= upperBound2; j++) {
                    //System.out.println("Checking " + i + " to " + j);
                    if(i==j){
                        //System.out.println("Overlap found at " + i);
                        total++;
                        break aapje;
                    }
                }
            }
        }
        System.out.println(total);
    }

    public static void main(String[] args) {
        new Day4();
    }
}
