package nl.aoc;

import nl.aoc.data.FileReader;

import java.util.ArrayList;
import java.util.List;

public class Day6 {

    protected final List<String> input = FileReader.getFile("/day6.txt");

    public Day6() {
        partOne();
        partTwo();
    }
    public void partOne() {
        for(String s : input) {
            for(int i = 0; i < s.length(); i++) {
                String sub = s.substring(i, i + 4);

                System.out.println("sub " + sub);
                if(!check(sub)) {
                    System.out.println("Found! " + sub + " index " + (i + 4));
                    break;
                }
            }
        }
    }
    //write a lambda function that returns the average number of a list
    public int getAverage(List<Integer> nums) {
        return nums.stream().mapToInt(Integer::intValue).sum() / nums.size();
    }

    public void partTwo() {
        for(String s : input) {
            for(int i = 0; i < s.length(); i++) {
                String sub = s.substring(i, i + 14);
                System.out.println("sub " + sub);
                if(!check(sub)) {
                    System.out.println("Found! " + sub + " index " + (i + 14));
                    break;
                }
            }
        }
    }
    public boolean check(CharSequence g) {
        for (int i = 0; i < g.length(); i++) {
            for (int j = i + 1; j < g.length(); j++) {
                if (g.charAt(i) == g.charAt(j)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static void main(String[] args) {
        new Day6();
    }
}
