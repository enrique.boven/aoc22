package nl.aoc;

import nl.aoc.data.FileReader;

import java.util.List;

public class Day8 {

    protected final List<String> input = FileReader.getFile("/day8.txt");

    int[][] grid = new int[input.size()][input.size()];

    public Day8() {
        int currentRow = 0;
        int currentColumn = 0;
        //caching input into 2d array
        for (String s : this.input) {
            for (char c : s.toCharArray()) {
                int value = Integer.parseInt(String.valueOf(c));
                //System.out.println("Column: " + currentColumn + " Row: " + currentRow + " Value: " + value);
                grid[currentRow][currentColumn] = value;
                currentColumn++;
            }
            currentRow++;
            currentColumn = 0;
        }
        //loop trough the inner grid and add if a tree is visible from any direction
        int total = 0;
        for (int x = 1; x < grid.length - 1; x++) {
            for (int y = 1; y < grid[x].length - 1; y++) {
                total += (canSee(x, y, Direction.UP)
                        || canSee(x, y, Direction.DOWN)
                        || canSee(x, y, Direction.LEFT)
                        || canSee(x, y, Direction.RIGHT)) ? 1 : 0;
            }
        }
        //add edges since they are always visible
        total += (grid.length * 2) + ((grid.length - 2) * 2);
        System.out.println("Total: " + total);

        System.out.println("====================================");
        int highest = 0;

        for (int x = 1; x < grid.length - 1; x++) {
            for (int y = 1; y < grid[x].length - 1; y++) {
                if(getScore(x, y) > highest) {
                    highest = getScore(x, y);
                }
            }
        }
        System.out.println("Highest: " + highest);
    }

    public int getScore(int row, int column) {

        int up = 0;
        int right = 0;
        int down = 0;
        int left = 0;

        //check up
        for (int i = row - 1; i >= 0; i--) {
            up++;
            if (grid[i][column] >= grid[row][column])
                break;
        }

        System.out.println("UP HAS " + up);

        //check right
        for (int i = column + 1; i < grid.length; i++) {
            right++;
            if (grid[row][i] >= grid[row][column])
                break;
        }
        System.out.println("RIGHT HAS " + right);

        //check down
        for (int i = row + 1; i < grid.length; i++) {
            down++;
            if (grid[i][column] >= grid[row][column])
                break;
        }

        System.out.println("DOWN HAS " + down);
        //check left
        for (int i = column - 1; i >= 0; i--) {
            left++;
            if (grid[row][i] >= grid[row][column])
                break;
        }
        System.out.println("LEFT HAS " + left);
        return (up * right * down * left);
    }

    public boolean canSee(int row, int column, Direction direction) {

        boolean can;
        int amountToScan;
        int count;

        switch (direction) {
            default:
            case UP:
                amountToScan = row;
                count = 0;
                for (int i = row - 1; i >= 0; i--) {
                    count += grid[i][column] < grid[row][column] ? 1 : 0;
                }
                can = amountToScan == count;
                break;
            case DOWN:
                amountToScan = (grid.length - 1) - row;
                count = 0;
                for (int i = row + 1; i < grid.length; i++) {
                    count += grid[i][column] < grid[row][column] ? 1 : 0;
                }
                can = amountToScan == count;
                break;
            case LEFT:
                amountToScan = column;
                count = 0;
                for (int i = column + -1; i >= 0; i--) {
                    count += grid[row][i] < grid[row][column] ? 1 : 0;
                }
                can = amountToScan == count;
                break;
            case RIGHT:
                amountToScan = (grid.length - 1) - column;
                count = 0;
                for (int i = column + 1; i < grid.length; i++) {
                    count += grid[row][i] < grid[row][column] ? 1 : 0;
                }
                can = amountToScan == count;
                break;
        }
        return can;
    }

    public enum Direction {
        RIGHT, LEFT, UP, DOWN
    }

    public static void main(String[] args) {
        new Day8();
    }
}
