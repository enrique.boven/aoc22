package nl.aoc;

import nl.aoc.data.FileReader;
import nl.aoc.accesories.Choices;
import nl.aoc.accesories.GameResult;

import java.util.List;

public class Day2 {

    protected final List<String> input = FileReader.getFile("/day2.txt");
    private int score = 0;

    public Day2() {
        partOne();
        score = 0;
        partTwo();
    }

    public void partOne() {
        for (String s : input) {
            String[] game = s.split(" ");
            //PART 1:
            doCheck(Choices.valueOf(game[0]), Choices.valueOf(game[1]));
        }
        System.out.println(score);
    }

    public void partTwo() {
        for (String s : input) {
            String[] game = s.split(" ");
            //PART 2:
            checkInput(Choices.valueOf(game[0]), GameResult.valueOf(game[1]));
        }
        System.out.println(score);
    }


    //part 2
    public void checkInput(Choices opponent, GameResult state) {
        switch (state) {
            //you win
            case Z:
                score += (6 + (opponent.equals(Choices.A) ? Choices.Y.getScore() : opponent.equals(Choices.B) ? Choices.Z.getScore() : Choices.X.getScore()));
                break;
            //you draw
            case Y:
                score += (3 + (opponent.equals(Choices.A) ? Choices.X.getScore() : opponent.equals(Choices.B) ? Choices.Y.getScore() : Choices.Z.getScore()));
                break;
            //you lose
            case X:
                score += (opponent.equals(Choices.A) ? Choices.Z.getScore() : opponent.equals(Choices.B) ? Choices.X.getScore() : Choices.Y.getScore());
                break;
        }
    }
    //part 1
    public void doCheck(Choices opponent, Choices you) {
        switch (opponent) {
            case A:
                this.score += you.equals(Choices.X) ? (Choices.X.getScore() + 3): you.equals(Choices.Y) ? (Choices.Y.getScore() + 6) : Choices.Z.getScore();
                break;
            case B:
                this.score += you.equals(Choices.X) ? (Choices.X.getScore()) : you.equals(Choices.Y) ? (Choices.Y.getScore() + 3) : (Choices.Y.getScore() + 6);
                break;
            case C:
                this.score += you.equals(Choices.X) ? (Choices.X.getScore() + 6) : you.equals(Choices.Y) ? (Choices.Y.getScore()) : (Choices.Z.getScore() + 3);
                break;
        }
    }
    public static void main(String[] args) {
        new Day2();
    }
}
