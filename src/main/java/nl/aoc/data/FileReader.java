package nl.aoc.data;

import nl.aoc.Day1;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class FileReader {
    public static List<String> getFile(String path) {
        return new BufferedReader(new InputStreamReader(Day1.class.getResourceAsStream(path), StandardCharsets.UTF_8)).lines().toList();
    }
}
